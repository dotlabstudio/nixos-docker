{
  config,
  pkgs,
  lib,
  ...
}: {
    
  # enable spice services
    services.spice-vdagentd.enable = true;
    services.spice-webdavd.enable = true;
    services.qemuGuest.enable = true;

  # virtualbox guest additions
  # virtualisation.virtualbox.guest.enable = true;
  #  virtualisation.virtualbox.guest.x11 = true;

  # vmware
  # virtualisation.vmware.guest.enable = true;

}
