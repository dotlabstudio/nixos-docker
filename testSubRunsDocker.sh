#!/usr/bin/env bash

# this script automates my test run for a new configuration.nix

rm -rf nixos-docker

git clone https://gitlab.com/dotlabstudio/nixos-docker.git

sudo cp nixos-docker/configuration.nix /etc/nixos

sudo nix-channel --update

sudo nixos-rebuild switch --upgrade


echo "Please reboot machine with the new configuration.nix"

