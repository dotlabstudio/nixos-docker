{ config, pkgs,lib, ... }:

{

   # setup navidrome music server

   # enable navidrome
   services.navidrome.enable = true;

   # enable some personal settings
   services.navidrome.settings = 
                 {
                    #MusicFolder = "/mnt/music";
                    #DataFolder  = "/mnt/navidrome/data";

                    LastFM.Enabled	= true;
                    LastFM.Secret	= "secret code";

                    ListenBrainz.Enabled = true;

                    Spotify.ID = "secret code";

                    Spotify.Secret = "secret code";
                    
                 };

   

 
}
