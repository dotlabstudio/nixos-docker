{ config, pkgs, ... }:

{
  
  # setup nextcloud on nixos

  # from the wiki - https://nixos.wiki/wiki/Nextcloud
  services.nextcloud = {
              enable = true;
              package = pkgs.nextcloud27;

              # make nextcloud load faster
              configureRedis = true;
              caching.apcu = false;

              extraApps = with config.services.nextcloud.package.packages.apps; {
                                  inherit news contacts calendar tasks;
                          };

              extraAppsEnable = true;

              hostName = "localhost";
              config.adminpassFile = "${pkgs.writeText "adminpass" "test123"}";

              # use local mail
              extraOptions = {
                    mail_smtpmode = "sendmail";
                    mail_sendmailmode = "pipe";
                   };

              # increase max upload limits
              phpOptions = {
                   upload_max_filesize = "16G";
                   post_max_size = "16G";
                  };
                   
            };



}
