# NixOS docker server

This repo is for a quick setup for a docker server in NixOS with same type of flat structure in my nixos repo.

Edit the bootloader.nix (bootloader.nix) to match the new hardware.

Edit configuration.nix and users.nix to suit your needs.

Only basic software is installed, the default shell being fish, tailscale is enabled, firewall is disable and ssh server is setup.

All the docker stuff is already setup and watchtower is also setup as a systemd service.

To run docker stuff, I would use a docker directory in the user home directory and setup the docker-compose.yaml files there.

```
mkdir -p docker/portainer/data
```

and just run:

```
docker compose up -d
```

from there.

## Change username with vim

```
:%s/changeme/yourusername/g
```


in normal mode

The letter s stands for substitute.

The keyword % will target the entire file instead of the current line.

The flag g means “global”: more than one occurrence is targeted. Without it, only the first occurrence in the file (or the in line) would be replaced.

# Nixos rolling

```
sudo nix-channel --list

sudo nix-channel --add https://channels.nixos.org/nixos-unstable nixos

sudo nixos-rebuild switch --upgrade
```

from https://nixos.org/manual/nixos/stable/#sec-upgrading



