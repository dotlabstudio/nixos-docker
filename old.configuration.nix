# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./hardware-acceleration.nix
      ./bootloader.nix
      ./hostname.nix
      ./locale.nix
      ./users.nix
      ./virtual.nix
      ./docker.nix
      ./watchtower.nix
    ];
  
  # hardware specific section - move to bootloader.nix 

  # copy the original hardware specific section to bootloader.nix file

  #networking.hostName = "nixos-server"; # Define your hostname. Change this for different machines
  # moved hostname to hostname.nix module now 

  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Australia/Melbourne";

  # locale section move to locale.nix
  # Select internationalisation properties.

  # tty console settings
   console = {
    packages=[ pkgs.terminus_font ];
    font="${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
    useXkbConfig = true; # use xkbOptions in tty.
  };
  
  # Configure keymap in X11
  #services.xserver = {
  #  layout = "au";
  #  xkbVariant = "";
  #};
  # old keyboard configuration name changed to below

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "au";
    variant = "";
  };
  
  # user section move to users.nix with default shell being fish
  # Define a user account. Don't forget to set a password with ‘passwd’.
  programs.fish.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    atuin

    clevis
    #Automated Encryption Framework

    git
    gnugrep
    gnupg
    htop

    libva
    # hardware Video Acceleration

    neofetch
    ripgrep
    signify
    tealdeer
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    yt-dlp
    
    # test os security
    lynis
    chkrootkit
    #ossec
    #crowdsec

  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # enable tailscale
    services.tailscale.enable = true;

  # enable faster reboots
    systemd.extraConfig = "DefaultTimeoutStopSec=10s";

  # security improvements
    services.sysstat.enable = true;

  # enable auditd
    security.auditd.enable = true;
    security.audit.enable = true;
    security.audit.rules = [
        "-a exit,always -F arch=b64 -S execve"
    ];

  # enable out of memory stuff
    services.earlyoom = {
      enable = true;
      freeSwapThreshold = 2;
      freeMemThreshold = 2;
      #extraArgs = [
      #    "-g" "--avoid '^(X|plasma.*|konsole|kwin)$'"
      #    "--prefer '^(electron|libreoffice|gimp)$'"
      #];
    };

  # use latest mainline kernel
    boot.kernelPackages = pkgs.linuxPackages_latest;

  # enable laptop services
    services.acpid.enable = true;
    services.thermald.enable = true;
    services.fwupd.enable = true;

  # create a strong and complex password beforehand 
    security.sudo.wheelNeedsPassword = false;

  # enable GVFS to work multiple filesystems
    services.gvfs.enable = true;

  # lock down nix access
  #  nix.allowedUsers = [ "@wheel" ];
  # name changes to
  
  # The option `nix.allowedUsers' defined in `/etc/nixos/configuration.nix' has been renamed to `nix.settings.allowed-users
    nix.settings.allowed-users = [ "@wheel" ];

  #  enable ssd trim 
     services.fstrim.enable = true;

  #  setup tang Server for binding data to network presence
     services.tang.enable = true;

  #    Whitelist a list of address prefixes. Preferably, internal addresses should be used.
     services.tang.ipAddressAllow = [
                                      "192.168.0.0/24"
                                    ];


  # cleaning the nix store

  # Optimise the store 
    nix.settings.auto-optimise-store = true;

  # Automatic Garbage Collection
    nix.gc = {
                automatic = true;
                dates = "weekly";
                options = "--delete-older-than 7d";
        };

  # automatic upgrades
    system.autoUpgrade.enable = true;
    system.autoUpgrade.allowReboot = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
    system.copySystemConfiguration = true;

  # virtual machines move to virtual.nix 
  
  # Enable the OpenSSH daemon.
  #  services.openssh.enable = true;
    services.openssh = {
      enable = true;
      settings.LogLevel = "VERBOSE";
      banner = "

#################################################################
#                   _    _           _   _                      #
#                  / \  | | ___ _ __| |_| |                     #
#                 / _ \ | |/ _ \ '__| __| |                     #
#                / ___ \| |  __/ |  | |_|_|                     #
#               /_/   \_\_|\___|_|   \__(_)                     #
#                                                               #
#  You are entering into a secured area! Your IP, Login Time,   #
#   Username has been noted and has been sent to the server     #
#                       administrator!                          #
#   This service is restricted to authorized users only. All    #
#            activities on this system are logged.              #
#  Unauthorized access will be fully investigated and reported  #
#        to the appropriate law enforcement agencies.           #
#################################################################

Welcome Back Legend !

NixOS home lab

";
        extraConfig = "

        AllowTcpForwarding no

        ClientAliveCountMax 2

        Compression no

        MaxAuthTries 3

        MaxSessions 2

        TCPKeepAlive no
        
        AllowAgentForwarding no

        ";

};

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
    networking.firewall.enable = false;
  
  # Tell the firewall to implicitly trust packets routed over Tailscale:
  #  networking.firewall.trustedInterfaces = [ "tailscale0" ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
