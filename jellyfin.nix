{ config, pkgs, lib, ... }:

{
  
  # setup the jellyfin media server

  # enable jellyfin
  services.jellyfin.enable = true;

  # open firewall for jellyfin
  services.jellyfin.openFirewall = true;

  # enable jellyseer
  services.jellyseerr.enable = true;



  
}
